import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";

const useStyles = makeStyles({
  inputStyle: {
    backgroundColor: "white",
    maxWidth: 70,
    maxHeight: 40,
    "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
      borderColor: "gainsboro",
      borderRadius: 0,
    },
  },
});

export default function CustomInputField() {
  const classes = useStyles();

  return (
    <TextField variant="outlined" size="small" className={classes.inputStyle} />
  );
}
