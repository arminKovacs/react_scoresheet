import React from "react";
import TeamsTable from "./TeamsTable";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
      padding: 13,
    },
    accordionStyle: {
      margin: "10px 0px 10px 0px",
      "& .MuiAccordionSummary-root.Mui-expanded": {
        borderBottom: "1px lightgray solid",
      },
      "& .MuiAccordionSummary-content": {
        margin: 0,
      },
      "& .MuiAccordionSummary-root": {
        padding: 0,
        margin: "0px 30px 0px 30px",
      },
    },
    accordionDetailsStyle: {
      marginTop: 50,
      padding: "19px 25px 19px 25px;",
    },
  })
);

export default function Categories() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Accordion
        elevation={0}
        variant="outlined"
        square={true}
        className={classes.accordionStyle}
      >
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>
            Team AG mini Level 1
          </Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.accordionDetailsStyle}>
          <TeamsTable />
        </AccordionDetails>
      </Accordion>
      <Accordion
        elevation={0}
        variant="outlined"
        square={true}
        className={classes.accordionStyle}
      >
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>
            Team Coed youth Level 2
          </Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.accordionDetailsStyle}>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion
        elevation={0}
        variant="outlined"
        square={true}
        className={classes.accordionStyle}
      >
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>
            Group Stunt AG youth Level 5
          </Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.accordionDetailsStyle}>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion
        elevation={0}
        variant="outlined"
        square={true}
        className={classes.accordionStyle}
      >
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>
            Group Stunt Coed junior Level 3
          </Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.accordionDetailsStyle}>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
