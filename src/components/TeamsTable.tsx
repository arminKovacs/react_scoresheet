import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Paper,
} from "@material-ui/core";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import Icon from "@mdi/react";
import { mdiChevronDownBox, mdiCommentOutline } from "@mdi/js";
import TooltippedTableCell from "./TooltippedTableCell";
import CustomInputField from "./CustomInputField";
import { TeamDataContext } from "./context/TeamDataContext";

const useStyles = makeStyles({
  tableBodyStyle: {
    borderCollapse: "separate",
    borderSpacing: 0,
  },
  scoringHeader: {
    fontWeight: "bold",
    display: "table-caption",
    marginLeft: "35%",
    fontSize: "medium",
    paddingBottom: 5,
  },
  tableHeaderCell: {
    padding: "5px 0px 5px 0px",
    borderBottom: 0,
    lineHeight: 1,
  },
  tableHeaderCellTop: {
    padding: "5px 0px 5px 0px",
    lineHeight: 1,
    borderLeft: 0,
    borderRight: 0,
    borderTop: "3px lightgray solid",
    borderBottom: "3px lightgray solid",
  },
  tableHeaderCellLeftSide: {
    lineHeight: 1,
    padding: "5px 0px 5px 0px",
    borderLeft: "3px lightgray solid",
    borderRight: 0,
    borderTop: "3px lightgray solid",
    borderBottom: "3px lightgray solid",
  },
  tableHeaderCellRightSide: {
    lineHeight: 1,
    padding: "14px 0px 5px 0px",
    borderLeft: 0,
    borderRight: "3px lightgray solid",
    borderTop: "3px lightgray solid",
    borderBottom: "3px lightgray solid",
  },
  disabledInputStyle: {
    backgroundColor: "gainsboro",
    maxWidth: 70,
    maxHeight: 40,
    "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
      borderColor: "gainsboro",
    },
  },
  tableLeftCellStyle: {
    fontSize: "larger",
    textAlign: "center",
    borderLeft: "1px solid #ccc;",
    borderTop: "1px solid #ccc;",
    borderBottom: "1px solid #ccc;",
  },
  tableCellStyle: {
    fontSize: "larger",
    textAlign: "center",
    borderTop: "1px solid #ccc;",
    borderBottom: "1px solid #ccc;",
  },
  tableRightCellStyle: {
    fontSize: "larger",
    textAlign: "center",
    borderRight: "1px solid #ccc;",
    borderTop: "1px solid #ccc;",
    borderBottom: "1px solid #ccc;",
  },
  spacerStyle: {
    padding: "5px 0px 5px 0px",
    border: 0,
    backgroundColor: "white",
  },
});

export default function TeamsTable() {
  const classes = useStyles();

  const rows = useContext(TeamDataContext);

  return (
    <TableContainer component={Paper} elevation={0}>
      <Table aria-label="simple table" className={classes.tableBodyStyle}>
        <TableHead className={classes.scoringHeader}>
          Scoring Master Sheet
        </TableHead>
        <TableHead>
          <TableRow>
            <TableCell align="center" className={classes.tableHeaderCell}>
              Team Nr.
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCell}>
              Team Name
            </TableCell>
            <TableCell
              align="center"
              className={classes.tableHeaderCellLeftSide}
            >
              <TooltippedTableCell criteria={"Cheer"} point={"10/"} />
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCellTop}>
              <TooltippedTableCell criteria={"Stunts"} point={"25/"} />
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCellTop}>
              <TooltippedTableCell criteria={"Pyramid"} point={"25/"} />
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCellTop}>
              <TooltippedTableCell criteria={"Basket tosses"} point={"15/"} />
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCellTop}>
              <TooltippedTableCell criteria={"Tumbling"} point={"10/"} />{" "}
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCellTop}>
              <TooltippedTableCell criteria={"Flow"} point={"5/"} />
            </TableCell>
            <TableCell
              align="center"
              className={classes.tableHeaderCellRightSide}
            >
              Overall <br /> 10/
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCell}>
              <b>
                Total <br /> 100/
              </b>
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCell}>
              Comment
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCell}>
              Rank
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCell}>
              Sign
            </TableCell>
            <TableCell align="center" className={classes.tableHeaderCell}>
              Safety
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => {
            return row.teamNr !== 0 ? (
              <TableRow key={row.teamNr}>
                <TableCell
                  component="th"
                  scope="row"
                  className={classes.tableLeftCellStyle}
                >
                  {row.teamNr}
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  {row.teamName}
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <CustomInputField />
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <CustomInputField />
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <CustomInputField />
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <CustomInputField />
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <CustomInputField />
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <CustomInputField />
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <CustomInputField />
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <TextField
                    variant="outlined"
                    size="small"
                    disabled
                    className={classes.disabledInputStyle}
                  />
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <Icon size={1} path={mdiCommentOutline} />
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <TextField
                    variant="outlined"
                    size="small"
                    disabled
                    className={classes.disabledInputStyle}
                  />
                </TableCell>
                <TableCell className={classes.tableCellStyle}>
                  <RadioButtonUncheckedIcon />
                </TableCell>
                <TableCell className={classes.tableRightCellStyle}>
                  <Icon size={1} path={mdiChevronDownBox} />
                </TableCell>
              </TableRow>
            ) : (
              <TableRow key={rows.indexOf(row)}>
                <TableCell className={classes.spacerStyle}></TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
