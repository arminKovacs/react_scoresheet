import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Tooltip } from "@material-ui/core";
import Icon from "@mdi/react";
import { mdiInformation } from "@mdi/js";

const useStyles = makeStyles({
  tooltipStyle: {
    backgroundColor: "white",
    fontSize: "12px",
    color: "black",
    border: "1px solid #ccc!important;",
    maxWidth: 100,
    textAlign: "center",
  },
  iconStyle: {
    marginBottom: 4,
    marginLeft: 1,
  },
});

export default function TooltippedTableCell(props: any) {
  const tooltipMessage: string = "Helpful information appears about scoring";
  const classes = useStyles();

  return (
    <div>
      {props.criteria}
      <Tooltip
        title={tooltipMessage}
        classes={{ tooltip: classes.tooltipStyle }}
        placement="top-start"
      >
        <Icon size={0.7} path={mdiInformation} className={classes.iconStyle} />
      </Tooltip>
      <br />
      {props.point}
    </div>
  );
}
