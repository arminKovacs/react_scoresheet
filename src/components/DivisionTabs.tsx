import React, { useState } from "react";
import { Tabs, Tab } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  tabsStyle: {
    marginTop: 0,
    marginBottom: 20,
    borderBottom: "1px solid #e8e8e8",
  },
  indicatorStyle: {
    height: 4,
    backgroundColor: "#44C3DE",
  },
  labelStyle: {
    fontWeight: "bold",
    fontSize: 16,
    minWidth: "33%",
  },
});

export default function DivisionTabs() {
  const classes = useStyles();
  const [value, setValue] = useState();

  const handleChange = (event: React.ChangeEvent<{}>, newValue: any) => {
    setValue(newValue);
  };

  return (
    <Tabs
      className={classes.tabsStyle}
      classes={{ indicator: classes.indicatorStyle }}
      value={value}
      variant="scrollable"
      onChange={handleChange}
      textColor="primary"
    >
      <Tab
        label="Cheerleading"
        className={classes.labelStyle}
        component={Link}
        to="/categories"
      />
      <Tab
        label="Performance Cheer divisons"
        className={classes.labelStyle}
        component={Link}
        to="/dead-link"
      />
      <Tab
        label="Other category's divisions"
        className={classes.labelStyle}
        component={Link}
        to="/dead-link"
      />
    </Tabs>
  );
}
