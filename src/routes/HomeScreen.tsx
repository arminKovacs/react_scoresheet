import React, { FC } from "react";
import DivisionTabs from "../components/DivisionTabs";
import Categories from "../components/Categories";
import {
  Typography,
  Container,
  Box,
  Card,
  CardContent,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Route } from "react-router-dom";

const useStyles = makeStyles({
  cardRoot: {
    margin: 13,
    paddingLeft: 5,
    paddingRight: 5,
  },
  topContentStyle: {
    marginTop: 30,
    marginBottom: 30,
    marginLeft: 35,
    marginRight: 35,
    padding: 0,
    "&:last-child": {
      paddingBottom: 5,
    },
  },
  bottomContentStyle: {
    padding: 15,
    "&:last-child": {
      paddingBottom: 45,
    },
  },
  boxStyle: {
    marginBottom: 0,
  },
});

const HomeScreen: FC = () => {
  const classes = useStyles();

  return (
    // sets left & right margin
    <Container maxWidth={false}>
      <Card className={classes.cardRoot} elevation={0}>
        <CardContent className={classes.topContentStyle}>
          <Typography>
            <Box
              mb={3}
              fontWeight="fontWeightBold"
              fontSize={16}
              className={classes.boxStyle}
            >
              Christmas Cheer Cup
            </Box>
          </Typography>
          <Typography component="p">
            <Box mb={3} className={classes.boxStyle} fontSize={14}>
              Munich, Germany
              <br />
              15 December, 2020
            </Box>
          </Typography>
        </CardContent>
      </Card>
      <Card className={classes.cardRoot} elevation={0}>
        <CardContent className={classes.bottomContentStyle}>
          <DivisionTabs />
          <Route component={Categories} path="/categories"/>
        </CardContent>
      </Card>
    </Container>
  );
};

export default HomeScreen;
